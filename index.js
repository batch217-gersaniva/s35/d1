const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;


// [SECTION] MongoDB connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.pzjdphc.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true, 
	useUnifiedTopology : true
});


let db = mongoose.connection;
//db.on --> preloaded codes on mongoose
db.on("error", console.error.bind(console, "connection error"));

//db.once --> if successful
db.once("open", () => console.log("We're connected to the cloud database"));

// [Section] Mongoose Schemas
// It determines the structure of our documents to be stored in the database
//It acts as our data blueprint and guide
// it will only consider the required task   ----> task ex. firstName, lastName 


const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},

	status: {
		type: String,
		default: "pending"
	}
})


// [Section] Models

const Task = mongoose.model("Task", taskSchema);





// [Section] Creation of to do list routes 

app.use(express.json());
// allows your app to read from forms
app.use(express.urlencoded({extended:true}));




// Creating a new task

// Business Logic
/*




*/



app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name} , (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})
})



//Get Method

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// Activity


const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "Username is required"]
	},

	password : {
		type: String,
		required: [true, "Password is required"]
	}

})


const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username, password: req.body.password} , (err, result) => {
		if(result != null && result.username == req.body.username && result.password == req.body.password){
			return res.send("Duplicate found");
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered.");
				}
			})
		}
	})
})




app.listen(port, () => console.log(`Server running at port ${port}`));